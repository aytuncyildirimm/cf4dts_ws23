<img src="https://pad.gwdg.de/uploads/08e0f09e-ed2d-463f-8f92-e7ea3c661d18.png" width="40%">

![cf4dt_winter_badge](https://img.shields.io/badge/CF4DTs-WiSe_2023-brightgreen)

Prof. Dr. Julia Kowalski\
Nazanin Bagherinejad\
Dipankul Bhattacarya

contact: bagherinejad@mbd.rwth-aachen.de

# Computational Foundations of Digital Twin Technologies
Welcome to the repository for our course project! This document provides essential information on getting started with GitHub, scheduling your presentation, and submitting your final project.

## Exercise 10

As part of the last exercise, we ask you to:
1. Schedule an appointment for your project presentation.
2. Submit an initial workflow for your project.

### 1. Schedule Your Presentation

1. Clone the repository as explained in the [Getting Started with Git](#getting-started-with-git) section.
2. Choose an available time slot from the table below.
3. Edit this README file to include your name in the respective time slot.
4. Commit your changes and push to the repository (**main branch**).


|| Date | Time | Participant 1 | Participant 2 (optional) |
|-| ---------- | :-------: | ---------- | --------- | 
|1| 29.01.2024 | 8:30 - 9:00 | Bekir Tosun  |              |               
|2| 29.01.2024 | 9:00 - 9:30 |             |              |               
|3| 29.01.2024 | 9:30 - 10:00 |             |              |               
|4| 01.02.2024 | 10:30 - 11:00 | Ijaz Aslamsha |           |              |               
|5| 01.02.2024 | 11:00 - 11:30 |            |              |               
|6| 01.02.2024 | 11:30 - 12:00 |            |              |               
|7| 05.02.2024 | 8:30 - 9:00 |             |              |               
|8| 05.02.2024 | 9:00 - 9:30 |             |              |               
|9| 05.02.2024 | 9:30 - 10:00 |             |              |               
|10| 08.02.2024 | 10:30 - 11:00 |            |              |                
|11| 08.02.2024 | 11:00 - 11:30 |            |              |               
|12| 08.02.2024 | 11:30 - 12:00 |            |              |                             

Happy presenting! :rocket:

### 2. Submit a Project Workflow

Create a new branch for your project as explained in the [Getting Started with Git](#getting-started-with-git) section. Provide a preliminary structure of how you would like to approach the topic.

- What are the main work packages that you have identified for the project?
- What are you planning to present?

Prepare a **Markdown file** containing your workflow. Commit and push your branch to the repository.

### Important Notes
- Make sure to push your initial commits ([schedule](#1-schedule-your-presentation) and [workFlow](#2-submit-a-project-workflow)) **no later than 08.01.2024**.

- Please note that we do **not** expect any kind of implementation at this point in time.

- If you encounter any issues or have questions, feel free to reach out.

## Getting started with Git

If you are not familiar with Git, here is quick guide to get you started. For more information and troubleshooting, please refer to the official [GitLab documentations](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

#### Register on RWTH GitLab

If you haven't already, please register on the GitLab instance of RWTH Aachen by visiting [RWTH GitLab](https://git.rwth-aachen.de/) and following the sign-up instructions.

#### Clone the Repository
To start working on your project, clone this repository to your local machine using the following command.

```bash
git clone <repository_url>
```

#### Create a Branch
Create a branch for your project work to keep the main branch clean. Use a branch name that reflects your project.

```bash
git checkout -b your-branch-name
```

#### Work on Your Project

Make changes, add files, and commit your work to your branch.

```bash
# Add changes to staging area
git add .

# Commit changes with a descriptive message
git commit -m "Your descriptive commit message"
```

#### Push to the Repository
Once you're ready to submit, push your branch to this repository.

```bash
git push -u origin your-branch-name
```

Good luck with your projects! :four_leaf_clover:
